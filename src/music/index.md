---
title: Music
---

## Songs in constructed languages

### Ŋarâþ Crîþ

* [<cite lang="art-x-ncv7">cenvel cjana</cite>](https://www.youtube.com/watch?v=JbCMl4hHXUI) (<time datetime="2019-01-25">2019-01-25</time>, v7)
* [<cite lang="art-x-ncv7">cerecaþa artfaþo</cite>](https://www.youtube.com/watch?v=XS63_lIlD3A) (<time datetime="2020-04-19">2020-04-19</time>, v7)
* [<cite lang="en">Synthesizer V Studio conlang test 1</cite>](https://www.youtube.com/watch?v=Y7fIijdCr-g) (<time datetime="2020-07-17">2020-07-17</time>, v7)
* [<cite lang="art-x-ncv7">#</cite>](https://www.youtube.com/watch?v=d02NQgi7Ee8) (<time datetime="2021-12-23">2021-12-23</time>, v7)
* [<cite lang="en">Conlang Test #2</cite>](https://www.youtube.com/watch?v=TG3-HauOyRY) (<time datetime="2022-01-03">2022-01-03</time>, v9)
* [<cite lang="art-x-ncv9">naðesos nelčon tfełor</cite>](https://www.youtube.com/watch?v=k4ndOV1yfcw) (<time datetime="2022-10-29">2022-10-29</time>, v9)
* <cite lang="art-x-ncv9">crînas tfel darłit geðates cemi’ve</cite> ([YT](https://www.youtube.com/watch?v=OK2i9Qq-YGw), [NND](http://www.nicovideo.jp/watch/so42907163)) (<time datetime="2023-11-02">2023-11-02</time>, v9)
* [<cite lang="art-x-ncv9">asnelerþ</cite>](https://www.youtube.com/watch?v=l94sIhUMvO4) (<time datetime="2024-06-30">2024-06-30</time>, v9)
* [<cite lang="art-x-ncv9">naldenôr</cite>](https://youtu.be/ErMgtf9lTOU) (<time datetime="2024-12-31">2024-12-31</time>, v9)

### Other conlangs

* [<cite lang="art">nerta virnu iri kana kapuuhaki</cite>](https://www.youtube.com/watch?v=r7dimbjVao4) (<time datetime="2018-12-02">2018-12-02</time>, Varta Avina)
* [<cite lang="art">Denúl nupos ṫarara</cite>](https://www.youtube.com/watch?v=G23IoEUVFig) (<time datetime="2018-12-25">2018-12-25</time>, Ḋraħýl Rase)
* [<cite lang="art-x-v3-arx0as">kalte e blenz</cite>](https://www.youtube.com/watch?v=EVoOS-38URQ) (<time datetime="2019-07-06">2019-07-06</time>, Arka)

## Trash heap

* [<cite lang="zxx">TEEJT_hA_zNXs</cite>](https://www.youtube.com/watch?v=MBnFRcV8hE8) (<time datetime="2022-12-31">2022-12-31</time>)
* [<cite lang="en">Fart of the Ugue</cite>](https://gitlab.com/Kyarei/fotu) (WIP) – A set of (4/12) fugues on ‘fictional language’ songs.
  * [\#1](https://www.youtube.com/watch?v=ow7-hp0Sfas) on YouTube
