---
title: Home
---

Hi, I’m <b>+merlan \#flirora</b>, a human being.

I write code (and occasionally get some money out of it), and my preferred language for programming is Rust. Some things I’ve made include:

* [Caxton], a mod that implements improved TTF and OTF font support for <cite>Minecraft: Java Edition</cite>
  * [fdsm]: a pure-Rust implementation of multi-channel signed distance field generation written for Caxton 0.5.0
* [f9i], a program that automatically inflects Ŋarâþ Crîþ v9 words
* [mscz2svp], a plugin for MuseScore that exports a score to a Synthesizer V Studio project
* [Fickle], a game where you guess the title of a ‘fictional language’ song every day

[Caxton]: https://modrinth.com/mod/caxton
[fdsm]: https://gitlab.com/Kyarei/fdsm
[f9i]: https://gitlab.com/ncv9/f9i
[mscz2svp]: https://gitlab.com/ncv9/mscz2svp
[Fickle]: https://kyarei.gitlab.io/fickle/

Aside from that, I’ve created a number of [constructed languages], with the best known being [Ŋarâþ Crîþ].

I also enjoy writing music. I’ve written a few songs in conlangs and voiced them using [Synthesizer V Studio].

I’ve also played <cite>Minecraft</cite> since 2012 and still follow it closely.

[constructed languages]: https://en.wikipedia.org/wiki/Constructed_language
[Ŋarâþ Crîþ]: https://ncv9.flirora.xyz
[Synthesizer V Studio]: https://dreamtonics.com/synthesizerv/

## FAQ

### What’s your real name?

+merlan \#flirora, pronounced \[meɹlan fliɹowa\].

The name is from Ŋarâþ Crîþ; the <i>+</i> is called a <i lang="art-x-ncv9">tor</i>
and indicates a surname, while the <i>\#</i> is called a <i lang="art-x-ncv9">carþ</i>
and indicates a given name.

#### No, what’s your *legal* name?

I prefer not to list it here, as I identify more with the name I gave myself.
If you know it, then you know.

### Pronouns?

I prefer to be referred with <i>they</i>/<i>them</i>.

## Links to elsewhere

* Mastodon: [@flirora@vocalounge.cafe](https://vocalounge.cafe/@flirora)
* GitLab: [Kyarei](https://gitlab.com/Kyarei)
* GitHub: [bluebear94](https://github.com/bluebear94)
* YouTube: [@flirora](https://www.youtube.com/@flirora)
* Discord: flirora
* Old site: [old.flirora.xyz](https://old.flirora.xyz)
* Email: **This has now moved.** (given name, without the <i lang="art-x-ncv9">carþ</i>)@(given name).xyz

<details>
<summary>PGP key for my email (<a href="https://keys.openpgp.org/vks/v1/by-fingerprint/4ADBDB7786DA95AA998F106F01D919341897765D">Download</a>)</summary>
<pre>
-----BEGIN PGP PUBLIC KEY BLOCK-----

xsDNBGeuwnkBDADV9pso+xUy3YA2H6PG+cXjXSP6EMaQBHvYyZZLq6+igE4mFijT
H7c0NNlWSxgIC6eRS8HecKDfnnSjO//zjao0zbh8E7fhrFf7mhqCw/MjFbfV1u+z
hQZ37ZH6geATpYazOMiLz93IbXG9NwXbY1U4cq+v/Fe40R+b0NiWLWFQEYvJkWir
VyA6jwCySMSN3id9T7TdJbN4ayvtVJShp+fsgau1TUhmtHPApyq2jJtPet6Ib/QN
CrYbwZ0Qzb0u6JxuRqa7oDulf799iawHFcK0qFJAoVjjJc8ShP4BcZFiM9VXs7Cj
eX/S/avaUvOvK/Z7aItlPa8soYq7gr5AAfNvhAR+Ab5DfIsju98rJGgcGfQIj1Wq
olHm0godMDYxrLSXpCvZRYYVmpqXcxOS58PEk+Gf12lA/Bpj/SU2nTMIxnnnKrN7
EULRw1paHERpMs5Z5Kl2mBxKUKaFsf3KMxVzUsgt/qjpIZf2K9x7Cwg3fvcQGY2O
NzgGR2qOwtFYzz8AEQEAAc0mK21lcmxhbiAjZmxpcm9yYSA8Zmxpcm9yYUBmbGly
b3JhLnh5ej7CwQ0EEwEIADcWIQRK29t3htqVqpmPEG8B2Rk0GJd2XQUCZ67CegUJ
BaOagAIbAwQLCQgHBRUICQoLBRYCAwEAAAoJEAHZGTQYl3Zdp4gL/AxzROT2780t
VO+64ElgdLd1elzIYxtc+LM8qex7cQrCqZW5ik+rtz1CVm8AXEZs1y9iKTMkm4e9
kmIbqMxvggh+Oi6+Tzd532QOvenoscdO2T5ne5ZheGtZ6lQ6EmvF6AXOIMeRekfg
ZmJ6jrMsqvgo6J9fUnC07JVIZ0Xg5UASxHW7YklDA9vsQHI2ggfcYGipebcXnYD2
BdJiVHs/1gJABzVa5vL8JM9iDoftupcqO1obvEQA3WVP5ckX1UZbJc90zfAdbN0/
f2aD5R2NY7WZrBDTTxewX+J1XvsUEttIu8DE04xxuKUypufuaxHC0nRrSfvIxGkf
GNBORk1AeYNulCpqL8eWzFH8qFMcoBTUwwOBdWinFTRkwaZ97MIscdoq03fI2zsg
JXCMay57zUvnWTCs3cMSsRszODQ4ImjvK7MgJgIp9qW+2200pWjPdpNPakTtdMi9
vKFJRzbw9BDQaNBNAa6IjqqTg6xFPMNZnbFtDbdL0iGOTUpXFufNAM7AzQRnrsJ6
AQwAmpLeTJOlCzdKSIq6yEWqFY5HM+vNX7EbDsHKXSLlVIHvKAFDMuWwUCp9LpTk
n5UIsw4iOUBKWs2pJDjfV5QKljBLSmnp/y0AXRlQ1reeyAJ5nsL1JAKUODFIoBCb
aaOyRGFNOeMW/3QGTgdkaOFskC5dhKonfoKFxUV86NZwu9OPJtJiw6Azc4Ag6g3q
s/wSF5OC46Joom85oKdUuaGWnu92vJo7WneFyxlLsNsD5A+eNN2N1xNta3D7c6Km
QQNV9jFwzg4DoAmH5j8p0gMcgHg8OIzRMNBpl/9az7L6hvWK9sgtHMkEbWzpV/Hk
K+x0H94gmZCN+M2j71u0o8rH9jKIqyF8CPSjMafbbI7rfvaIozezWf9v8b7+fhl7
EoZJBtCxq5+Uz03c6tT7H/4ts+mrAx9i0URrX6efy8QeqqsURhxqTSRG8jcLVJ0q
4iDej4tHhhZfBZATqn48IiXB7LAm4QOrTUUalz+WrmSM9fTMn0A7eIUkAR3bKqOr
qPHJABEBAAHCwPwEGAEIACYWIQRK29t3htqVqpmPEG8B2Rk0GJd2XQUCZ67CegUJ
BaOagAIbDAAKCRAB2Rk0GJd2XdZ8C/0bSdezvnAAiDJr59NfPPZ+95ijurCx8z8h
tyB0r/ggu9c2b1iALv9LIJvKY4LIFOlsPUK+abd9JdnqH9pJRfQrqAo8ZvbYZT3o
QsPMdb7Cg77IhiGdHdJxK2hpnl80IuGkHGUTQEOBewUNTJjx+GWfSl1sfRYlEPsq
h98QNsXrQblI84M5c8j7oxLp1mExHfKQtdu+15OeX4Y4dcNURc+UkqHTd+5Fo8Q6
Wf6j8OiR1L4kYjeM4aOIgbg2fFaSyApRcAdYpQONQjUau0hiKqeOROw9cshzQgmK
CZFw4CmLtOZlzXRZkMnDxkP7n2/Z/pG/WPCd/KIOgMvWD6f7AEBehvD8lobnIrkN
1yhxR+saJ2XwgxrDRtumXoIOue8F3VLYkCy08xbNEzoED/f4pfSYzq9sRZQ942Ag
EhZrL8iUNYHB+FMGt+OKIpRp9I0Ec9VkR9ZgdduRUWfqoV+Fn1Ps3Qp8HRrRTVPN
oGo3QhUNCAVoPpqyOTQohQ64k+TDVAk=
=85+1
-----END PGP PUBLIC KEY BLOCK-----

</pre>
</details>
