import lume from "lume/mod.ts";
import attributes from "lume/plugins/attributes.ts";
import base_path from "lume/plugins/base_path.ts";
import code_highlight from "lume/plugins/code_highlight.ts";
import date from "lume/plugins/date.ts";
import esbuild from "lume/plugins/esbuild.ts";
import eta from "lume/plugins/eta.ts";
import feed from "lume/plugins/feed.ts";
import filter_pages from "lume/plugins/filter_pages.ts";
import transformImages from "lume/plugins/transform_images.ts";
import inline from "lume/plugins/inline.ts";
import katex from "lume/plugins/katex.ts";
import search from "lume/plugins/search.ts";
import minifyHTML from "lume/plugins/minify_html.ts";
import svgo from "lume/plugins/svgo.ts";
import footnote from "npm:markdown-it-footnote";

const site = lume({
    src: './src',
    location: new URL("https://flirora.xyz"),
    prettyUrls: false,
});

site.use(attributes());
site.use(base_path());
site.use(code_highlight());
site.use(date());
site.use(esbuild());
site.use(eta());
site.use(feed());
site.use(filter_pages({ fn: _page => true }));
site.use(transformImages());
site.use(inline());
site.use(search());
site.use(minifyHTML({ extensions: [".html", ".css", ".js"] }));
site.use(svgo());
site.use(katex({
  options: {
    output: 'mathml',
    delimiters: [
      { left: "\\(", right: "\\)", display: false },
      { left: "\\[", right: "\\]", display: true },
    ]
  },
}))
site.hooks.addMarkdownItPlugin(footnote);

site.loadAssets([".css", ".js", ".svg"]);
site.copyRemainingFiles(
    (path: string) => path.startsWith("/assets/"),
  );

export default site;
